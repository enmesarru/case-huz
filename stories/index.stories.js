import React from "react";
import { storiesOf } from "@storybook/react";

import Spine from "../src/ui";
import PlayStore from "../src/ui/playStore";

// PROJECT-CONTEXT
import initMockup, { Provider as MockupProvider } from "../src/@mockup/project";


const mockup = initMockup();
const stories = storiesOf("index", module);

stories.add("default", () => (
  <MockupProvider>
    <Spine />
  </MockupProvider>
));

stories.add("play store page theme", () => (
  <MockupProvider>
    <PlayStore />
  </MockupProvider>
));