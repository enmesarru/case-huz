import React from "react";
import { fade, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import SettingsIcon from "@material-ui/icons/Settings";
import { Button, Divider } from "@material-ui/core";
import ContactSupportIcon from "@material-ui/icons/ContactSupport";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuSettings: {
    backgroundColor: theme.palette.common.white,
    color: "#737373",
  },
  toolbar: {
    minHeight: 48,
    "&:nth-child(1)": {
      marginLeft: 40,
    },
  },
  divider: {
    height: 30,
    margin: "9px 5px",
  },
  rightMenu: {
    display: "flex",
    "& > h6": {
      padding: "0 10px 0 10px",
      height: 48,
      lineHeight: "48px",
      transition: "all 0.1s",
    },
    "& > h6:hover": {
      background: "#eee",
    },
  },
  appBarLeftMenu: {
    textTransform: "none",
    height: 48,
  },
}));

export default function SearchBar() {
  const classes = useStyles();

  const menuId = "primary-search-account-menu";

  return (
    <div className={classes.grow}>
      <AppBar position="static" className={classes.menuSettings}>
        <Toolbar className={classes.toolbar}>
          <div>
            <Button className={classes.appBarLeftMenu}>
              <Typography variant="h6">Games</Typography>
              <ExpandMoreIcon />
            </Button>
            <Button className={classes.appBarLeftMenu}>
              Subcategories
              <ExpandMoreIcon />
            </Button>
          </div>
          <Divider
            orientation="vertical"
            flexItem
            className={classes.divider}
          />
          <div className={classes.rightMenu}>
            <Typography variant="subtitle2" noWrap>
              Home
            </Typography>
            <Typography variant="subtitle2" noWrap>
              Top charts
            </Typography>
            <Typography variant="subtitle2" noWrap>
              New releases
            </Typography>
          </div>
          <div className={classes.grow} />
          <div>
            <Button
              edge="end"
              aria-controls={menuId}
              aria-haspopup="true"
              variant="outlined"
              size="small"
              color="inherit"
              style={{ margin: "0 10px" }}
            >
              <ContactSupportIcon />
            </Button>
            <Button
              edge="end"
              aria-controls={menuId}
              aria-haspopup="true"
              variant="outlined"
              size="small"
              color="inherit"
            >
              <SettingsIcon />
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
