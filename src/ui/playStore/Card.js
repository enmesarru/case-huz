import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import { Avatar } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 330,
  },
  cardContent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  },
  large: {
    width: 50,
    height: 50,
  },
});

export default function GameCard({ picture, title, company, star, avatar }) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={title}
          height={185}
          image={picture}
          title={title}
        />
        <CardContent style={{ padding: 0 }}>
          <div className={classes.cardContent}>
            <Avatar src={avatar} variant="square" className={classes.large} />
            <div>
              <Typography
                gutterBottom
                variant="subtitle1"
                component="div"
                style={{ margin: "0" }}
              >
                {title}
              </Typography>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                component="div"
                style={{ fontSize: "13px" }}
              >
                {company}
              </Typography>
              <Rating
                name="half-rating"
                defaultValue={star}
                precision={0.5}
                size="small"
                style={{ fontSize: "13px" }}
              />
            </div>
          </div>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
