import React from "react";
import SearchBar from "./SearchBar";

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import {
  Box,
  Button,
  Container,
  CssBaseline,
  Grid,
  GridList,
  GridListTile,
  Typography,
  withStyles,
} from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import Card from "./Card";

const themeLight = createMuiTheme({
  palette: {
    background: {
      default: "#eee",
    },
  },
});

const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.common.white,
    backgroundColor: green[400],
    "&:hover": {
      backgroundColor: green[700],
    },
  },
}))(Button);

const data = [
  {
    title: "Konuşan Tom Arkadaşlarım",
    picture:
      "https://play-lh.googleusercontent.com/0SDORBUCjtBKwPbSIEEs0uQionMMNwLzsvx10P2Xn31aiMJRmyfybZBisxVOe-LTrw=w330-h185",
    company: "Outfit7 Limited",
    star: 4.2,
    avatar:
      "https://play-lh.googleusercontent.com/nbeMRpwHHLL12aI0SJQ03GDHMM-fjWXkcoukaJ1_qQJfiN08I2gzQT25M-SKSF7Cy0A=s50",
  },
  {
    title: "Konuşan Tom Arkadaşlarım",
    picture:
      "https://play-lh.googleusercontent.com/sAmzBQMaAuKwR0knYI8ebWwKOJYTa3TZ7c1LKQXFqi8fQ0hozWgF1IlhO1l-JlvH3A=w330-h185",
    company: "Outfit7 Limited",
    star: 4.2,
    avatar:
      "https://play-lh.googleusercontent.com/xOgV-lsJ0C3367TI_ECmWk0Xg27IYRM_srFNe-WC1fYUnzgLIm8Ysz3igpLRkT1M2tI=s50",
  },
  {
    title: "Konuşan Tom Arkadaşlarım",
    picture:
      "https://play-lh.googleusercontent.com/0SDORBUCjtBKwPbSIEEs0uQionMMNwLzsvx10P2Xn31aiMJRmyfybZBisxVOe-LTrw=w330-h185",
    company: "Outfit7 Limited",
    star: 4.2,
    avatar:
      "https://play-lh.googleusercontent.com/nbeMRpwHHLL12aI0SJQ03GDHMM-fjWXkcoukaJ1_qQJfiN08I2gzQT25M-SKSF7Cy0A=s50",
  },
];

function News() {
  return (
    <>
      <Box
        display="flex"
        flexDirection="row"
        justifyContent="space-between"
        style={{ paddingTop: "40px" }}
      >
        <Typography variant="h5" noWrap>
          New & updated games
        </Typography>
        <ColorButton>See more</ColorButton>
      </Box>
      <GridList
        cols={0}
        cellHeight={"auto"}
        style={{
          width: "100%",
          maxHeight: 320,
          flexWrap: "nowrap",
        }}
      >
        {data.map((n) => {
          return (
            <GridListTile key={n}>
              <Card key={n} num={n} {...n} />
            </GridListTile>
          );
        })}
      </GridList>
    </>
  );
}

function LightWeightGames() {
  return (
    <>
      <Box display="flex" flexDirection="row" justifyContent="space-between">
        <Typography variant="h5" noWrap>
          Low on space ?
        </Typography>

        <ColorButton>See more</ColorButton>
      </Box>
      <Typography variant="subtitle1">Lightweight games</Typography>
      <GridList
        cols={0}
        cellHeight={"auto"}
        style={{
          width: "100%",
          maxHeight: 320,
          flexWrap: "nowrap",
        }}
      >
        {data.map((n) => {
          return (
            <GridListTile key={n}>
              <Card key={n} num={n} {...n} />
            </GridListTile>
          );
        })}
      </GridList>
    </>
  );
}

export default (props) => {
  return (
    <MuiThemeProvider theme={themeLight}>
      <CssBaseline />
      <SearchBar />
      <Container>
        <News />
        <LightWeightGames />
      </Container>
    </MuiThemeProvider>
  );
};
