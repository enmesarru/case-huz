import React, { useRef, useState } from "react";
import DatGui, { DatSelect, DatNumber } from 'react-dat-gui'
import { spine } from './spine-canvas'
import { loadSkeleton, resize } from './utils'
import "react-dat-gui/dist/index.css";


function success(response) {
  console.info(response)
}

function error(response) {
  console.error(response)
}


let renderOptions = {
  skeleton: null,
  state: null,
  bounds: null,
  lastFrameTime: Date.now() / 1000
}

const skeletons = ["powerup", "raptor", "spineboy"]

export default (props) => {
  const canvas = useRef(null)
  const skeletonRenderer = useRef(null)
  const assetManager = useRef(new spine.canvas.AssetManager())

  const [options, setOptions] = useState({
    skeletonName: "spineboy",
    animationName: "death",
  })

  const [skeletonAnimations, setSkeletonAnimations] = useState([])

  React.useLayoutEffect(() => {
    canvas.current.width = document.body.clientWidth; //document.width is obsolete
    canvas.current.height = document.body.clientHeight; //document.height is obsolete
  }, [])

  React.useEffect(() => {
    let context = canvas.current.getContext("2d");

    skeletonRenderer.current = new spine.canvas.SkeletonRenderer(context);
    skeletonRenderer.current.debugRendering = true;
    skeletonRenderer.current.triangleRendering = false;
    let skeletonRoute = `${options.skeletonName}/${options.skeletonName}-pro`
    assetManager.current.loadText(`assets/${skeletonRoute}.json`, success, error);
    assetManager.current.loadText(`assets/${skeletonRoute}.atlas`, success, error);
    assetManager.current.loadTexture(`assets/${skeletonRoute}.png`, success, error);

    const load = time => {
      let isLoaded = assetManager.current.isLoadingComplete()
      if (isLoaded) {
        let { animations } = JSON.parse(assetManager.current.assets[`assets/${skeletonRoute}.json`])

        const animationList = Object.keys(animations)
        let selectedAnimationName = options.animationName

        if (!animationList.includes(selectedAnimationName)) {
          selectedAnimationName = animationList[0]
        }
        let data = loadSkeleton(options.skeletonName, selectedAnimationName, "default", assetManager.current);
        renderOptions.skeleton = data.skeleton;
        renderOptions.state = data.state;
        renderOptions.bounds = data.bounds;

        requestAnimationFrame(render);
        setSkeletonAnimations(animationList)

      } else {
        requestAnimationFrame(load);
      }
    }

    const render = time => {
      var now = Date.now() / 1000;
      var delta = now - renderOptions.lastFrameTime ;
      renderOptions.lastFrameTime = now;
      context = canvas.current.getContext("2d");

      resize(renderOptions.bounds, canvas.current, context);

      context.save();
      context.setTransform(1, 0, 0, 1, 0, 0);
      context.fillStyle = "#173753";
      context.fillRect(0, 0, canvas.current.width, canvas.current.height);
      context.restore();

      renderOptions.state.update(delta);
      renderOptions.state.apply(renderOptions.skeleton);
      renderOptions.skeleton.updateWorldTransform();
      skeletonRenderer.current.draw(renderOptions.skeleton);

      requestAnimationFrame(render);
    }
    requestAnimationFrame(load);

  }, [options]);

  return (
    <div>
      <canvas ref={canvas}  width="1000" height="700"></canvas>
      <DatGui data={options} onUpdate={setOptions}>
        <DatSelect
          path="skeletonName"
          label="Skeletion Name"
          options={skeletons}
        />
        <DatSelect
          path="animationName"
          label="Animations"
          options={skeletonAnimations}
        />
        <DatNumber
          path="delta"
          label="Time Multiplier"
        />
      </DatGui>
    </div>
  )
};